terraform {
  # required_version = ">= 1.3.8"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }

  }
}



provider "aws" {
  #region = "ca-central-1"
  #profile = "terraform-user"
}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "lordcjenkins"
    key            = "jjtech/terraform.tfstate"
    region         = "ca-central-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "lordcarterdb"
  }
 }




